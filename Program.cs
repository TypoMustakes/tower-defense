using System;
using LibTower;
using LibUI;
using Fields;

namespace Tower_Defense
{
	class Program
	{
		public static void Main(string[] args)
        {
            BufferManipulator.GenerateMap();
            BufferManipulator.PlaceTowers();
        }
    }
}
