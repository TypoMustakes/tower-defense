using LibUI;

namespace LibTower
{
	
	class Character
	{
		private int x, y;
		private char displayCharacter;
		private ConsoleColor color;
		
		public int X
		{
			get
			{
				return this.x;
			}
			set
			{
				BufferManipulator.EraseCharacter(this);
				this.x = value;
				BufferManipulator.PlaceCharacter(this);
			}
		}
		public int Y
		{
			get
			{
				return this.y;
			}
			set
			{
				this.y = value;
				BufferManipulator.PlaceCharacter(this);
			}
		}
		
		public char DisplayCharacter //a megjelenítéskor kirajzolandó karakter
		{
			get
			{
				return this.displayCharacter;
			}
			set
			{
				this.displayCharacter = value;
				BufferManipulator.PlaceCharacter(this);
			}
		}
		
		public ConsoleColor Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
				BufferManipulator.PlaceCharacter(this);
			}
		}
		
		public Character(int x = 0, int y = 2, char displayCharacter = '#', ConsoleColor color = ConsoleColor.White)
		{
			this.Y = y;
			this.X = x;
			this.DisplayCharacter = displayCharacter;
			this.Color = color;
			
			BufferManipulator.PlaceCharacter(this);
		}
	}
	
	class Tower
	{
		public Tower(int x = 0, int y = 4, char displayCharacter = 'O')
		{
			this.X = x;
			this.Y = y;
			this.DisplayCharacter = displayCharacter;
			this.Color = color;
			Random rangePicker = new Random();
			this.Range = (Ranges)rangePicker.Next(0, 3);
		}
		
		private Ranges range;
		
		public Ranges Range
		{
			get
			{
				return this.range;
			}
			set
			{
				this.range = value;
				
				if (this.Range == Ranges.Low)
				{
					this.Color = ConsoleColor.Green;
				}
				else if (this.Range == Ranges.Medium)
				{
					this.Color = ConsoleColor.Cyan;
				}
				else if (this.Range == Ranges.High)
				{
					this.Color = ConsoleColor.Blue;
				}
				
				BufferManipulator.PlaceTower(this);
			}
		}
		
		public enum Ranges
		{
			Low,
			Medium,
			High
		}
		
		private int x, y;
		private char displayCharacter;
		private ConsoleColor color;
		
		public int X
		{
			get
			{
				return this.x;
			}
			set
			{
				BufferManipulator.EraseTower(this);
				this.x = value;
				BufferManipulator.PlaceTower(this);
			}
		}
		public int Y
		{
			get
			{
				return this.y;
			}
			set
			{
				this.y = value;
				BufferManipulator.PlaceTower(this);
			}
		}
		
		public char DisplayCharacter //a megjelenítéskor kirajzolandó karakter
		{
			get
			{
				return this.displayCharacter;
			}
			set
			{
				this.displayCharacter = value;
				BufferManipulator.PlaceTower(this);
			}
		}
		
		public ConsoleColor Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
				BufferManipulator.PlaceTower(this);
			}
		}
	}
	
	class Monster
    {   
        public Monster(int x = 0, int y = 2, char displayCharacter = 'M', ConsoleColor color = ConsoleColor.Red)
        {
            this.X = x;
            this.Y = y;
            this.DisplayCharacter = displayCharacter;
            this.Color = color;
			
            BufferManipulator.PlaceMonster(this);
        }
		
        public HealthState Health
        {
            get
            {
                return this.Health;
            }
            set
            {
                this.Health = value;
                BufferManipulator.EraseMonster(this);
            }
        }
		
        public enum HealthState
        {
            Alive,
            Dead
        }
		
        private int x, y;
        private char displayCharacter;
        private ConsoleColor color;
		
        public int X
        {
			get
            {
				return this.x;
            }
			set
            {
				BufferManipulator.EraseMonster(this);
				this.x = value;
				BufferManipulator.PlaceMonster(this);
            }
        }
        public int Y
        {
			get
            {
				return this.y;
            }
			set
            {
				this.y = value;
				BufferManipulator.PlaceMonster(this);
            }
        }
		
        public char DisplayCharacter //a megjelenítéskor kirajzolandó karakter
        {
			get
            {
				return this.displayCharacter;
            }
			set
            {
				this.displayCharacter = value;
				BufferManipulator.PlaceMonster(this);
            }
        }
		
        public ConsoleColor Color
        {
			get
            {
				return this.color;
            }
			set
            {
				this.color = value;
				BufferManipulator.PlaceMonster(this);
            }
        }
    }
}
