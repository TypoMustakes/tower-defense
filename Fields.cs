using LibTower;
namespace Fields;

static class GlobalFields
{
	public static Character[,] CharactersInBuffer = new Character[MapWidth, MapHeight];
	public static Tower[,] TowersInBuffer = new Tower[MapWidth, MapHeight];
	public static Monster[,] MonstersInBuffer = new Monster[MapWidth, MapHeight];
	
	public const int MapWidth = 51; //50 + 1
	public const int MapHeight = 6; //5 + 1
	
	public static Tower[] Towers = new Tower[5];
	
	public static void InitializeTowers()
	{
		for (int i = 0; i < Towers.Length; i++)
		{
			Towers[i] = new Tower();
		}
	}
}
