#+PROPERTY: header-args :tangle yes :tangle "~/Projects/tower-defense/Program.cs" :comments org"

#+begin_src c
  using System;
  using LibTower;
  using LibUI;
  using Fields;

  namespace Tower_Defense
  {
#+end_src

* Program class

#+begin_src c
  class Program
  {
#+end_src

** Main

#+begin_src c
          public static void Main(string[] args)
          {
              BufferManipulator.GenerateMap();
              BufferManipulator.PlaceTowers();
          }
      }
  }
#+end_src
