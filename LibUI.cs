using LibTower;
using Fields;

namespace LibUI
{
	
	static class BufferManipulator
	{
		public static void GenerateMap()
		{
			Console.Clear();
			
			for (int i = 0; i < 50; i++)
			{
				Console.SetCursorPosition(i,0);
				Console.Write("-");
				Console.SetCursorPosition(i,4);
				Console.Write("-");
			}
			
			Console.SetCursorPosition(0, 4); //a tornyok lerakásához
		}
		
		public static void PlaceCharacter(Character character)
		{
			int tmpX = Console.CursorLeft;
			int tmpY = Console.CursorTop;
			
			Console.SetCursorPosition(character.X, character.Y);
			Console.ForegroundColor = character.Color;
			Console.Write(character.DisplayCharacter);
			Console.ResetColor();
			
			Console.SetCursorPosition(tmpX, tmpY);
			
			GlobalFields.CharactersInBuffer[character.X, character.Y] = character; 
		}
		
		public static void PlaceTower(Tower character)
		{
			int tmpX = Console.CursorLeft;
			int tmpY = Console.CursorTop;
			
			Console.SetCursorPosition(character.X, character.Y);
			Console.ForegroundColor = character.Color;
			Console.Write(character.DisplayCharacter);
			Console.ResetColor();
			
			Console.SetCursorPosition(tmpX, tmpY);
			
			GlobalFields.TowersInBuffer[character.X, character.Y] = character; 
		}
		
		public static void PlaceMonster(Monster character)
		{
			int tmpX = Console.CursorLeft;
			int tmpY = Console.CursorTop;
			
			Console.SetCursorPosition(character.X, character.Y);
			Console.ForegroundColor = character.Color;
			Console.Write(character.DisplayCharacter);
			Console.ResetColor();
			
			Console.SetCursorPosition(tmpX, tmpY);
			
			GlobalFields.MonstersInBuffer[character.X, character.Y] = character; 
		}
		
		public static void EraseCharacter(Character character)
		{
			Console.SetCursorPosition(character.X, character.Y);
			Console.Write(" ");
			
			GlobalFields.CharactersInBuffer[character.X, character.Y] = null;
		}
		
		public static void EraseTower(Tower character)
		{
			Console.SetCursorPosition(character.X, character.Y);
			Console.Write("-");
			
			GlobalFields.TowersInBuffer[character.X, character.Y] = null;
		}
		
		public static void EraseMonster(Monster character)
		{
			Console.SetCursorPosition(character.X, character.Y);
			Console.Write(" ");
			
			GlobalFields.MonstersInBuffer[character.X, character.Y] = null;
		}
		
		public static void PlaceTowers()
		{
			GlobalFields.InitializeTowers();
			
			Character arrow = new Character(0, 5, '^', ConsoleColor.Yellow);
			
			for (int i = 0; i < GlobalFields.Towers.Length; i++)
			{
				int index = 0;
				bool cancel = false;
				
				while (!cancel)
				{
					string Input = Console.ReadKey(true).Key.ToString();
					
					if (Input == "RightArrow")
					{
						if (arrow.X + 1 <= GlobalFields.MapWidth)
						{
							arrow.X++;
						}
					}
					else if (Input == "LeftArrow")
					{
						if (arrow.X - 1 >= 0)
						{
							arrow.X--;
						}
					}
					else if (Input == "Spacebar")
					{
						if (GlobalFields.TowersInBuffer[arrow.X, arrow.Y - 1] == null)
						{
							index = arrow.X;
							cancel = true;
						}
						else
						{
							Warning("Ezen a helyen már van egy tornyod!");
						}
					}
				}
				
				GlobalFields.Towers[i].X = index;
				BufferManipulator.PlaceTower(GlobalFields.Towers[i]);
			}
		}
		
		public static void Warning(string message)
		{
			int tmpX = Console.CursorLeft;
			int tmpY = Console.CursorTop;
			
			Console.SetCursorPosition(0, Console.WindowHeight - 2);
			Console.Write($"{message}\nNyomj le egy gombot a folytatáshoz!");
			Console.ReadKey(true);
			
			Console.SetCursorPosition(0, Console.WindowHeight - 2);
			string eraser = string.Empty;
			for (int i = 0; i < Console.WindowWidth; i++)
			{
				eraser += " ";
			}
			Console.Write($"{eraser}\n{eraser}");
			Console.SetCursorPosition(tmpX, tmpY);
		}
	}
}
